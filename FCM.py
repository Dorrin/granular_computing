import numpy as np
from utils import norma_euclidean


def fcm(data, c, m=2, eps=1e-9):
    """
    Fuzzy c means algorithm implementation.

    Fuzzy c means is used to divide data into spacial-related clusters.
    :param data: Data we want to cluster.
    :param c: Amount of clusters.
    :param m: Dilution factor. Default to 2.
    :param eps: Precision of calculations. Default to 1e-9.
    :return: Final partition matrix, fuzzy cluster center matrix, list of quality factors
    """
    # np.random.seed(42) # testing purposes

    def gen_U():
        """
        Function for U matrix generation.

        This matrix indicates degree of membership points to sets.
        Matrix U is random and column-wise normalized.
        Values in U are from [0,1] interval.
        :return: Random normalized partition matrix.
        """
        matrix = np.random.random((c, data.shape[0]))
        return matrix / matrix.sum(0)

    def calculate_V(U):
        """
        Function for succesive V matrix calculation.

        V matrix indicates, where are located centers of sets.
        This function uses previously generated / calculated U matrix
        for result improval.
        :param U: Partition matrix.
        :return: Fuzzy cluster center matrix.
        """
        cols = data.shape[1]
        V = np.zeros((c, cols))
        for i in range(c):
            for j in range(cols):
                V[i][j] = np.dot(U[i, ...] ** m, data[..., j])
                V[i][j] /= (U[i] ** m).sum()
        return V

    def calculate_U(V):
        """
        Fuction for succesive calculation previously generated U matrix.

        This fuction uses previously calculated center matrix and distance
        function for results improval.
        :param V: Fuzzy cluster center matrix.
        :return: Partition matrix.
        """
        U = np.zeros((c, data.shape[0]))
        for i in range(c):
            for j in range(data.shape[0]):
                mian = 0
                for k in range(c):
                    mian += norma_euclidean(data[j], V[i]) / norma_euclidean(data[j], V[k])

                U[i][j] = 1. / mian
        return U / U.sum(0)

    def calculate_Q(U, V):
        """
        Function for quality factor calculation.

        Quality factor shows us, what is happening with clustering.
        If values of this function are decreasing, probably everything is fine.
        This function also indicates, where to break the loop.
        :param U: Partition matrix.
        :param V: Fuzzy cluster center matrix.
        :return: Quality factor.
        """
        Q = 0
        for i in range(c):
            for j in range(data.shape[0]):
                Q += U[i, j] ** m * norma_euclidean(data[j], V[i])
        return Q

    u = gen_U()
    v = calculate_V(u)
    q = calculate_Q(u, v)
    prev_q = q-1
    iters = 0
    q_list = [q]

    while abs(q - prev_q) > eps:
        iters += 1
        prev_q = q
        u = calculate_U(v)
        v = calculate_V(u)
        q = calculate_Q(u, v)
        q_list.append(q)
        print(q)

    print(iters)
    return u, v, q_list


if __name__ == "__main__":
    print(fcm(np.random.randint(101, size=(100, 2)), 3, 2, 1e-14))
