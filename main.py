from TS import takagi_sugeno
from FCM_parallel import fcm_parallel
import numpy as np

# example of main calculating file

if __name__ == "__main__":
    arr = np.loadtxt("data/housing.data")
    np.random.shuffle(arr)
    split_set = np.array_split(arr, 10)
    clust = {2: 1e-5, 3: 1e-4, 4: 1e-3, 5: 1e-2, 6: 1e-2, 7: 0.02}
    ms = [1.2, 1.6, 2, 2.4, 2.8]
    with open("data/housingu2.data", 'w') as f:

        for c in clust:
            print("================= c = " + str(c) + " ====================")

            for m in ms:
                results_l = 0
                results_t = 0
                cntl = 0
                cntt = 0
                print("================= m = " + str(m) + " ====================")
                f.write("c = " + str(c) + " | m = " + str(m) + "\n")
                for j in range(10):
                    test = split_set[j]
                    learn = np.concatenate([split_set[k] for k in range(10) if k != j])
                    res = takagi_sugeno(learn[..., 1:], learn[..., 0], test[..., 1:],
                                        test[..., 0], c, m=m, eps=clust[c])
                    # print(res[2])

                    if res[0] < 1e10:
                        results_l += res[0]
                        cntl += 1
                    if res[1] < 1e10:
                        results_t += res[1]
                        cntt += 1

                f.write("Usrednione: \n")
                f.write(str(results_l/cntl))
                f.write("\n")
                f.write(str(results_t/cntt))
                f.write("\n")
                f.write("=============================================================")
                f.write("\n")

    with open("data/housing_fcm.data", 'wb') as f:
        for c in clust:
            print("================= c = " + str(c) + " ====================")
            for m in ms:
                print("================= m = " + str(m) + " ====================")
                res = fcm_parallel(arr[..., 1:], c, m, 1e-5)
                f.write(np.compat.asbytes("c= " + str(c) + " | m = " + str(m) + "\n"))
                f.write(np.compat.asbytes('Centra: \n'))
                np.savetxt(f, res[1], fmt="%f")
                f.write(np.compat.asbytes('\n'))
                f.write(np.compat.asbytes("iter = " + str(len(res[2])) + "\n"))
                f.write(np.compat.asbytes("J = " + str(res[2][-1]) + "\n"))
                f.write(np.compat.asbytes("============================================================="))
                f.write(np.compat.asbytes("\n"))
