import numpy as np
from scipy.spatial.distance import pdist

def norma_euclidean(v1, v2):
    """
    Normalized Squared Euclidean Distance.

    Definition taken from wolfram doc:
    http://reference.wolfram.com/language/ref/NormalizedSquaredEuclideanDistance.html
    :param v1: First vector.
    :param v2: Second vector.
    :return: Squared euclidean distance (single number).
    """
    return 0.5 * np.linalg.norm((v1 - v1.mean()) - (v2 - v2.mean())) ** 2 / \
        (np.linalg.norm(v1 - v1.mean()) ** 2 + np.linalg.norm(v2 - v2.mean()) ** 2)

def euclidean(v1, v2):
    """
    Standard Euclidean Distance.

    Wrapper for funcion from scipy module
    :param v1: First vector.
    :param v2: Second vector.
    :return: Euclidean distance.
    """
    return pdist(np.array([v1, v2]), 'euclidean')

