import numpy as np
from FCM_parallel import fcm_parallel, calculate_U

def takagi_sugeno(x_l, y_l, x_t, y_t, c, m=2, eps=1e-9):
    """
    Takagi sugeno model.

    :param x_l: Data of learning set.
    :param y_l: Results from learning set.
    :param x_t: Data of test set.
    :param y_t: Results from test set.
    :param c: Amount of clusters.
    :param m: Dilution factor. Default to 2.
    :param eps: Precision of calculations. Default to 1e-9.
    :return:
    """
    x_learn = x_l
    y_learn = y_l
    x_test = x_t
    y_test = y_t

    def calculate_G(X, U):
        row = X.shape[0]
        col = X.shape[1]
        G = np.zeros([row, c * (col+1)])
        for i in range(row):
            for j in range(c):
                G[i, j * (col+1)] = U.transpose()[i][j] * 1.
                for k in range(col):
                    G[i, j*(col+1)-1+(k+2)] = U.transpose()[i][j] * X[i][k]
        return G

    def calculate_a(G, Y):
        A = np.dot(G.transpose(), G)
        A = np.linalg.inv(A)
        b = np.dot(G.transpose(), Y)
        return np.dot(A, b)

    def calculate_Y(G, a):
        row = G.shape[0]
        if len(a.shape) > 1:
            col = a.shape[1]
            Y = np.zeros([row, col])
            for i in range(row):
                for j in range(col):
                    Y[i, j] = np.dot(G[i], a[..., j])
        else:
            Y = np.zeros(row)
            for i in range(row):
                Y[i] = np.dot(G.transpose()[..., i], a)
        return Y

    u, v, q = fcm_parallel(x_learn, c, m, eps)
    g = calculate_G(x_learn, u)
    a = calculate_a(g, y_learn)
    y_learn_tmp = calculate_Y(g, a)
    q_learn = np.sqrt(np.sum((y_learn - y_learn_tmp)**2)/y_learn.shape[0])
    print()
    print(q_learn)

    u_t = calculate_U(v, c, x_test)
    g_t = calculate_G(x_test, u_t)
    a_t = calculate_a(g_t, y_test)
    y_test_tmp = calculate_Y(g_t, a_t)
    q_test = np.sqrt(np.sum((y_test - y_test_tmp) ** 2)/y_test.shape[0])
    print()
    print(q_test)
    print()

    return q_learn, q_test, v



