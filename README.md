# Granular Computing#

Repository with functions related to granular computing, based on lecture "An Introduction to Granular Computing".  
AGH University of Science and Technology, Faculty of Physics and Applied Computer Science, 2014/2015.  
Lecture taught by prof. Witold Pedrycz.