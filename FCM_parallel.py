import numpy as np
import multiprocessing as mp
from utils import norma_euclidean


def calculate_U(V, c, pdata):
    """
    Fuction for succesive calculation previously generated U matrix.

    Outside fcm_parallel because of pickle restriction.
    This fuction uses previously calculated center matrix and distance
    function for results improval.
    :param V: Fuzzy cluster center matrix.
    :param c: Amount of clusters.
    :param pdata: Part of data matrix.
    :return: Partition matrix.
    """
    U = np.zeros((c, pdata.shape[0]))
    for i in range(c):
        for j in range(pdata.shape[0]):
            mian = 0
            for k in range(c):
                mian += norma_euclidean(pdata[j], V[i]) / norma_euclidean(pdata[j], V[k])
            U[i][j] = 1. / mian
    return U / U.sum(0)

def calculate_V(U, c, m, pdata):
    """
    Function for succesive V matrix calculation.

    V matrix indicates, where are located centers of sets.
    This function uses previously generated / calculated U matrix
    for result improval.
    :param U: Partition matrix.
    :param c: Cluster
    :param m: Dilution factor.
    :param pdata: Part of data matrix.
    :return: Fuzzy cluster center matrix.
    """
    cols = pdata.shape[1]
    V = np.zeros((c, cols))
    for i in range(c):
        for j in range(cols):
            V[i][j] = np.dot(U[i, ...] ** m, pdata[..., j])
            V[i][j] /= (U[i] ** m).sum()
    return V

def calculate_Q(U, V, c, m, pdata):
    """
    Function for quality factor calculation.

    Outside fcm_parallel because of pickle restriction.
    Quality factor shows us, what is happening with clustering.
    If values of this function are decreasing, probably everything is fine.
    This function also indicates, where to break the loop.
    :param U: Partition matrix.
    :param V: Fuzzy cluster center matrix.
    :param c: Cluster
    :param m: Dilution factor. Default to 2.
    :return: Quality factor.
    """
    Q = 0
    for i in range(c):
        for j in range(pdata.shape[0]):
            Q += U[i, j] ** m * norma_euclidean(pdata[j], V[i])
    return Q

def fcm_parallel(data, c, m=2, eps=1e-9):
    """
    Fuzzy c means algorithm implementation.

    Fuzzy c means is used to divide data into spacial-related clusters.
    :param data: Data we want to cluster.
    :param c: Amount of clusters.
    :param m: Dilution factor. Default to 2.
    :param eps: Precision of calculations. Default to 1e-9.
    :return: Final partition matrix, fuzzy cluster center matrix, list of quality factors
    """
    np.random.seed(42) # testing purposes

    def gen_U():
        """
        Function for U matrix generation.

        This matrix indicates degree of membership points to sets.
        Matrix U is random and column-wise normalized.
        Values in U are from [0,1] interval.
        :return: Random normalized partition matrix.
        """
        matrix = np.random.random((c, data.shape[0]))
        return matrix / matrix.sum(0)

    u = gen_U()
    v = calculate_V(u, c, m, data)
    q = calculate_Q(u, v, c, m, data)
    prev_q = q-1
    iters = 0
    q_list = [q]
    pool = mp.Pool(processes=4)

    while abs(q - prev_q) > eps:
        iters += 1
        prev_q = q
        sdata = np.array_split(data, 4)
        res = [pool.apply_async(calculate_U, args=(v, c, sdata[i])) for i in range(4)]
        u = np.concatenate([p.get() for p in res], axis=1)
        su = np.array_split(u, 4, axis=1)
        v = calculate_V(u, c, m, data)
        #v = np.concatenate([pool.apply(calculate_V, args=(u, c, m, sdata[i])) for i in range(4)])
        res = [pool.apply_async(calculate_Q, args=(su[i], v, c, m, sdata[i])) for i in range(4)]
        q = sum([p.get() for p in res])
        q_list.append(q)
        #print(q)

    pool.close()

    print(iters)
    return u, v, q_list


if __name__ == "__main__":
    print(fcm_parallel(np.random.randint(101, size=(100, 2)), 2, 2, 1e-14))
